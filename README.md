---
## Set up venv and dependencies

1. Create a virtual environment.
2. Activate venv.
3. CMD "pip install -r requirements.txt".
---

## Run tests from Test_runner

1. CMD 'python -m unittest {YOUR_RELATIVE_PATH}\aqa_test_task\Test_runner\Run_suite.py'.
2. Navigate to '{YOUR_RELATIVE_PATH}\Test_runner\outputs'.
3. Open in any browser the HTML output file, Suite will store as many reports as desired, each report contains timestamp within file name.
4. Navigate each test and inspect executions details.
---

