import unittest
from authentication.user_token import user_access_token
from service import pageService
from random import choice

class testService(unittest.TestCase):
    def setUp(self):
        self.test_page = pageService(user_access_token)

    def test01_create_single_post(self):
        # store previous state
        posts = self.test_page.posts
        # perform create action
        r = self.test_page.create_post('Automated test content for test purposes')
        # output assertion
        self.assertTrue(r)
        self.assertTrue(len(self.test_page.posts) == len(posts)+1)
        print('New post was added.')
    def test02_update_single_post(self):
        # get available posts
        posts = self.test_page.posts
        # create post if there are no posts available.
        if len(posts) == 0:
            self.test_create_single_post()
            posts = self.test_page.posts
        # get random post to update
        post = choice(posts)
        post_id = post['id']
        # update_criteria
        update_criteria = 'Updated trough API \n {}'.format(post['message'])
        # perform update action
        r = self.test_page.update_post(post['id'], update_criteria)
        # output assertion
        if r is True:
            print('Post {} was updated.'.format(post_id))
            for post in self.test_page.posts:
                if post['id'] == post_id:
                    self.assertEqual(post['message'], update_criteria)
    def test03_delete_single_post(self):
        # get available posts
        posts = self.test_page.posts
        # create post if there are no posts available.
        if len(posts) == 0:
            self.test_create_single_post()
            posts = self.test_page.posts

        post_id = (choice(posts))['id']
        # perform delete action
        r = self.test_page.delete_post(post_id)
        # output assertion
        if r is True:
            print('Post {} was deleted.'.format(post_id))
        self.assertTrue(len(self.test_page.posts) == len(posts)-1)
    def test04_bulk_create_post(self):
        # store previous state
        posts = self.test_page.posts
        '''
        For this case I'll iterate over a fixed range (creating just 10 posts), however if it was a real test probally the iteration numbers
        will be dynamic and content will come from an external source such a json file
        '''
        count = 0
        for x in range(1,11):
            # perform create action
            r = self.test_page.create_post('Automated test content for test purposes iteration {}'.format(x))
            # output assertion
            self.assertTrue(r)
            self.assertTrue(len(self.test_page.posts) == len(posts) + x)
            count += 1
        print('{} New posts were added.'.format(count))
    def test05_bulk_update_posts(self):
        # get post for delete action
        update_actions = self.test_page.posts
        # perform update actions
        for idx, post in enumerate(update_actions):
            # update_criteria
            update_criteria = 'Updated trough API on iteration {} \n {}'.format(idx, post['message'])
            # perform update action
            r = self.test_page.update_post(post['id'], update_criteria)
            # output assertion
            if r is True:
                print('Post {} was updated.'.format(post['id']))
                for updated_post in self.test_page.posts:
                    if updated_post['id'] == post['id']:
                        self.assertEqual(updated_post['message'], update_criteria)
    def test06_delete_all_posts(self):
        # get post for delete action
        delete_actions = self.test_page.posts
        # perform delete action
        for idx, post in enumerate(delete_actions):
            r = self.test_page.delete_post(post['id'])
            if r is True:
                print('Post {} was deleted.'.format(post['id']))
        # output assertion
        self.assertFalse(self.test_page.posts)

    def tearDown(self):
        self.test_page.user.user_service.session.close()
        self.test_page.page_service.session.close()