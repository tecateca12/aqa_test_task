import requests
import json
from definitions import ROOT_DIR

with open('{}/authentication/user.json'.format(ROOT_DIR), 'r') as f:
    user = json.load(f)

with open('{}/authentication/app.json'.format(ROOT_DIR), 'r') as f:
    app = json.load(f)

access_token_url = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id={}&client_secret={}&fb_exchange_token={}".format(app['app_id'], app['app_secret'], user['user_token'])
r = requests.get(access_token_url)
access_token_info = r.json()
user_access_token = access_token_info['access_token']
