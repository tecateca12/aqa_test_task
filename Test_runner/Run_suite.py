import time
import unittest
from Tests import tests
from Test_runner import HTMLTestRunner
from definitions import ROOT_DIR

class TestSuite(unittest.TestCase):

    def test_suite_regression(self):
        smoke_test = unittest.TestSuite()
        smoke_test.addTests([
            unittest.defaultTestLoader.loadTestsFromTestCase(tests.testService)

        ])
        path = '{}\\Test_runner\\outputs'.format(ROOT_DIR)
        file ="%s_Test_Suite.html" %time.strftime('%Y%m%d_%H%M%S')
        outfile = open('{}\\{}'.format(path, file), "w", encoding="utf8")
        runner1 = HTMLTestRunner.HTMLTestRunner(
                stream=outfile,
                title='AQA_TEST_ASSIGNMENT',
                description='Test create/update/delete facebook posts.'
        )
        runner1.run(smoke_test)
        outfile.close()

if __name__ == '__main__':
    unittest.main()