import logging
import facebook

class userService():
    def __init__(self, user_access_token):
        self.user_service = facebook.GraphAPI(access_token=user_access_token, version="3.1")
    @property
    def associated_pages(self):
        return self.user_service.get_object("/me/accounts")

class pageService():
    def __init__(self, user_access_token):
        self.user_access_token = user_access_token
        self.user = userService(user_access_token)
        self.page_token = self.user.associated_pages['data'][0]['access_token']
        self.page_id = self.user.associated_pages['data'][0]['id']
        # init service instance
        self.page_service = facebook.GraphAPI(access_token=self.page_token, version="3.1")

    @property
    def posts(self):
        r = self.page_service.get_object(self.page_id, fields='posts')
        page_posts = []
        try:
            for post in r['posts']['data']:
                page_posts.append(post)
            return page_posts
        except:
            return []
    def create_post(self, post_content):
        try:
            self.page_service.put_object(self.page_id, 'feed', message=post_content)
            return True
        except Exception as e:
            logging.error(e)
            return False
    def delete_post(self,post_id):
        try:
            self.page_service.delete_object(post_id)
            return True
        except Exception as e:
            logging.error(e)
            return False
    def update_post(self, post_id, new_feed):
        args = {
            'message': new_feed,
            'access_token': self.user_access_token
        }
        try:
            self.page_service.request(post_id, post_args=args)
            return True
        except Exception as e:
            logging.error(e)
            return False